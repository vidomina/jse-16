package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
