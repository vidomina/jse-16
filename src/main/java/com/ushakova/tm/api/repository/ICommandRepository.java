package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
