package com.ushakova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showSystemInfo();

}
